const fs = require('fs');
const dirName = process.cwd();
const configPath = dirName + '/daeds-atomic.json';

const getConfig = function(skipOutputFile){
    if (!fs.existsSync(configPath)) {
        console.error('ERROR: The file daeds-atomic.json was not found i current directory.');
        return null;
    }

    const config = JSON.parse(fs.readFileSync(configPath));

    if (!config.folders || !config.folders.length) {
        console.log('ERROR: No folders defined in daeds-atomic.json');
        return null;
    }

    if (!config.output) {
        console.log('ERROR: No output defined in daeds-atomic.json');
        return null;
    }

    if (!config.fileExtensions || !config.fileExtensions.length) {
        console.log('ERROR: No fileExtensions defined in daeds-atomic.json');
        return null;
    }

    config.folders = config.folders.map(path => dirName + (/^\//.test(path) ? path : ('/' + path)));
    config.fileExtensions = config.fileExtensions.map(ext => ext.replace(/^\./, ''));
    
    if (!skipOutputFile) {
        config.output = dirName + (/^\//.test(config.output) ? config.output : ('/' + config.output));
        config.outputFolderPath = config.output.replace(/\/[a-z0-9\.\-_]+$/i, '');

        if (config.outputFolderPath && !fs.existsSync(config.outputFolderPath)) {
            console.log('ERROR: The path for output css file does not exist.');
            return null;
        }

        if (config.folders.includes(config.outputFolderPath) || config.folders.includes(config.outputFolderPath + '/')) {
            console.log('ERROR: Path for output css file must not be included in folders array.');
            return null;
        }
    }

    return config;
};

module.exports = getConfig;