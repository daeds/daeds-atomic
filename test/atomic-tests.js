const assert = require('assert');
const Atomic = require('../atomic');

var atomic = new Atomic();

beforeEach(() => {
    atomic.reset();
    atomic.config = {};
});

describe('generellt', () => {
    it('klass ska kunna vara mitt i en klasslista', () => {
        let str = '"d_inline m_10px po_static"';
        let result = atomic.process(str).getCss();
        let expected = '.d_inline { display: inline; }.m_10px { margin: 10px; }.po_static { position: static; }';

        assert.equal(result, expected);
    });

    it('klasser ska inte dupliceras', () => {
        let str = '"mb_10px mb_10px mb_10px"';
        let result = atomic.process(str).getCss();
        let expected = '.mb_10px { margin-bottom: 10px; }';

        assert.equal(result, expected);
    });

    it('ska kunna ange negativt numeriskt värde', () => {
        let str = '"ml_-20px"';
        let result = atomic.process(str).getCss();
        let expected = '.ml_-20px { margin-left: -20px; }';

        assert.equal(result, expected);
    });

    it('ska kunna ange underscore för mellanslag i värden', () => {
        let str = '"m_20px_10px"';
        let result = atomic.process(str).getCss();
        let expected = '.m_20px_10px { margin: 20px 10px; }';

        assert.equal(result, expected);
    });

    it('ska kunna ange procent i värden som kodas i klassnamnet', () => {
        let str = '"m_100%"';
        let result = atomic.process(str).getCss();
        let expected = '.m_100\\% { margin: 100%; }';

        assert.equal(result, expected);
    });

    it('ska kunna ange parenteser i värden som kodas i klassnamnet', () => {
        let str = '"trf_translateY(10px)"';
        let result = atomic.process(str).getCss();
        let expected = '.trf_translateY\\(10px\\) { transform: translateY(10px); }';

        assert.equal(result, expected);
    });

    it('ska kunna ange hash i värden som kodas i klassnamnet', () => {
        let str = '"color_#ccc"';
        let result = atomic.process(str).getCss();
        let expected = '.color_\\#ccc { color: #ccc; }';

        assert.equal(result, expected);
    });

    it('ska kunna ange kommatecken i värden som kodas i klassnamnet', () => {
        let str = '"bgc_rgba(0,0,0,1)"';
        let result = atomic.process(str).getCss();
        let expected = '.bgc_rgba\\(0\\,0\\,0\\,1\\) { background-color: rgba(0,0,0,1); }';

        assert.equal(result, expected);
    });

    it('ska kunna ange punkter i värden som kodas i klassnamnet', () => {
        let str = '"lh_1.5em"';
        let result = atomic.process(str).getCss();
        let expected = '.lh_1\\.5em { line-height: 1.5em; }';

        assert.equal(result, expected);
    });

    it('ska inte skriva ut något om bara prefix hittas', () => {
        let str = '"ml"';
        let result = atomic.process(str).getCss();
        let expected = '';

        assert.equal(result, expected);
    });

    it('ska inte skriva ut något om bara prefix och underscore hittas', () => {
        let str = '"ml_"';
        let result = atomic.process(str).getCss();
        let expected = '';

        assert.equal(result, expected);
    });

    it('ska kunna hantera flera anrop till process med samma klasser utan dubletter', () => {
        let str = '"m_50px"';
        
        atomic.process(str);
        atomic.process(str);

        let result = atomic.getCss();
        let expected = '.m_50px { margin: 50px; }'

        assert.equal(result, expected);
    });

    it('metoderna process och reset ska returnera this så att de kan chainas', () => {
        let result = atomic.reset().process('"m_10px"').getCss();
        let expected = '.m_10px { margin: 10px; }';

        assert.equal(result, expected);
    });

    it('ska spara undan alla responsiva klasser vid flera anrop till process med separat metod för att lägga till sist', () => {
        let result = atomic
            .process('"m_50px"')
            .process('"m_30px_at600px"')
            .process('"m_10px_at400px"')
            .getCss();
        
        let expected = '.m_50px { margin: 50px; }@media screen and (max-width:600px) { .m_30px_at600px { margin: 30px; }}@media screen and (max-width:400px) { .m_10px_at400px { margin: 10px; }}';

        assert.equal(result, expected);
    });

    it('ska även kunna skriva in klasser i enkelfnuttar', () => {
        let str = "'p_10px'";
        let result = atomic.process(str).getCss();
        let expected = '.p_10px { padding: 10px; }';

        assert.equal(result, expected);
    });

    it('ska kunna ange variabler i config att användas i klassnamn med prefix @', () => {
        let str = "'bgc_@bgColor'";

        atomic.config.variables = { bgColor : 'blue' };

        let result = atomic.process(str).getCss();
        let expected = '.bgc_\\@bgColor { background-color: blue; }';

        assert.equal(result, expected);
    });

    it('ska kunna ange variabler med versaler', () => {
        let str = "'bgc_@COLOR'";

        atomic.config.variables = { COLOR : 'blue' };

        let result = atomic.process(str).getCss();
        let expected = '.bgc_\\@COLOR { background-color: blue; }';

        assert.equal(result, expected);
    });

    it('flera variabler ska kunna användas i samma klassnamn', () => {
        let str = "'m_@top_@right_@bottom_@left'";

        atomic.config.variables = { top: '10px', right: '20px', bottom: '30px', left: '40px' };

        let result = atomic.process(str).getCss();
        let expected = '.m_\\@top_\\@right_\\@bottom_\\@left { margin: 10px 20px 30px 40px; }';

        assert.equal(result, expected);
    });

    it('ska kunna ange overrides för klassprefix i config', () => {
        let str = "'p_10px'";

        atomic.config.overrides = { p: 'margin' };
        atomic.setupOverrides();

        let result = atomic.process(str).getCss();
        let expected = '.p_10px { margin: 10px; }';

        assert.equal(result, expected);

        atomic.config.overrides = { p: 'padding' };
        atomic.setupOverrides();
    });

    it('ska kunna ange lista med klasser i config som alltid måste komma med i resultat', () => {
        let str = "'p_10px'";

        atomic.config.alwaysInclude = ['m_5px', 'po_static'];

        let result = atomic.process(str).getCss();
        let expected = '.p_10px { padding: 10px; }.m_5px { margin: 5px; }.po_static { position: static; }';

        assert.equal(result, expected);

        atomic.config.alwaysInclude = undefined;
    });

    it('ska lägga på !important om klassnamn slutar med utropstecken', () => {
        let str = "'m_10px!'";
        let expected = '.m_10px\\! { margin: 10px !important; }';
        let result = atomic.process(str).getCss();

        assert.equal(result, expected);
    });
});

describe('klassprefix', () => {
    let arr = [
        { test: 'm = margin',               source: '"m_10px"',         expected: '.m_10px { margin: 10px; }' },
        { test: 'mt = margin-top',          source: '"mt_10px"',        expected: '.mt_10px { margin-top: 10px; }' },
        { test: 'ml = margin-left',         source: '"ml_10px"',        expected: '.ml_10px { margin-left: 10px; }' },
        { test: 'mr = margin-right',        source: '"mr_10px"',        expected: '.mr_10px { margin-right: 10px; }' },
        { test: 'mb = margin-bottom',       source: '"mb_10em"',        expected: '.mb_10em { margin-bottom: 10em; }' },
        { test: 'p = padding',              source: '"p_10px"',         expected: '.p_10px { padding: 10px; }' },
        { test: 'pt = padding-top',         source: '"pt_10px"',        expected: '.pt_10px { padding-top: 10px; }' },
        { test: 'pl = padding-left',        source: '"pl_10px"',        expected: '.pl_10px { padding-left: 10px; }' },
        { test: 'pr = padding-right',       source: '"pr_10px"',        expected: '.pr_10px { padding-right: 10px; }' },
        { test: 'pb = padding-bottom',      source: '"pb_10em"',        expected: '.pb_10em { padding-bottom: 10em; }' },
        { test: 'radius = border-radius',   source: '"radius_5px"',     expected: '.radius_5px { border-radius: 5px; }' },
        { test: 'w = width',                source: '"w_100px"',        expected: '.w_100px { width: 100px; }' },
        { test: 'minw = min-width',         source: '"minw_800px"',     expected: '.minw_800px { min-width: 800px; }' },
        { test: 'maxw = max-width',         source: '"maxw_800px"',     expected: '.maxw_800px { max-width: 800px; }' },
        { test: 'h = height',               source: '"h_100px"',        expected: '.h_100px { height: 100px; }' },
        { test: 'minh = min-height',        source: '"minh_400px"',     expected: '.minh_400px { min-height: 400px; }' },
        { test: 'maxh = max-height',        source: '"maxh_400px"',     expected: '.maxh_400px { max-height: 400px; }' },
        { test: 'bw = border-width',        source: '"bw_5px"',         expected: '.bw_5px { border-width: 5px; }' },
        { test: 'btw = border-top-width',   source: '"btw_5px"',        expected: '.btw_5px { border-top-width: 5px; }' },
        { test: 'blw = border-left-width',  source: '"blw_5px"',        expected: '.blw_5px { border-left-width: 5px; }' },
        { test: 'brw = border-right-width', source: '"brw_5px"',        expected: '.brw_5px { border-right-width: 5px; }' },
        { test: 'bbw = border-bottom-width', source: '"bbw_5px"',       expected: '.bbw_5px { border-bottom-width: 5px; }' },
        { test: 'fs = font-size',           source: '"fs_15px"',        expected: '.fs_15px { font-size: 15px; }' },
        { test: 'lh = line-height',         source: '"lh_20px"',        expected: '.lh_20px { line-height: 20px; }' },
        { test: 'left = left',              source: '"left_50px"',      expected: '.left_50px { left: 50px; }' },
        { test: 'right = right',            source: '"right_50px"',     expected: '.right_50px { right: 50px; }' },
        { test: 'top = top',                source: '"top_20px"',       expected: '.top_20px { top: 20px; }' },
        { test: 'bottom = bottom',          source: '"bottom_20px"',    expected: '.bottom_20px { bottom: 20px; }' },
        { test: 'd = display',              source: '"d_block"',        expected: '.d_block { display: block; }' },
        { test: 'po = position',            source: '"po_absolute"',    expected: '.po_absolute { position: absolute; }' },
        { test: 'boxsizing = box-sizing',   source: '"boxsizing_border-box"', expected: '.boxsizing_border-box { box-sizing: border-box; }' },
        { test: 'f = float',                source: '"f_left"',         expected: '.f_left { float: left; }' },
        { test: 'b = border',               source: '"b_none"',         expected: '.b_none { border: none; }' },
        { test: 'bs = border-style',        source: '"bs_solid"',       expected: '.bs_solid { border-style: solid; }' },
        { test: 'bc = border-color',        source: '"bc_red"',         expected: '.bc_red { border-color: red; }' },
        { test: 'bt = border-top',          source: '"bt_none"',        expected: '.bt_none { border-top: none; }' },
        { test: 'bts = border-top-style',   source: '"bts_solid"',      expected: '.bts_solid { border-top-style: solid; }' },
        { test: 'btc = border-top-color',   source: '"btc_red"',        expected: '.btc_red { border-top-color: red; }' },
        { test: 'bl = border-left',         source: '"bl_none"',        expected: '.bl_none { border-left: none; }' },
        { test: 'bls = border-left-style',  source: '"bls_solid"',      expected: '.bls_solid { border-left-style: solid; }' },
        { test: 'blc = border-left-color',  source: '"blc_red"',        expected: '.blc_red { border-left-color: red; }' },
        { test: 'br = border-right',        source: '"br_none"',        expected: '.br_none { border-right: none; }' },
        { test: 'brs = border-right-style', source: '"brs_solid"',      expected: '.brs_solid { border-right-style: solid; }' },
        { test: 'brc = border-right-color', source: '"brc_red"',        expected: '.brc_red { border-right-color: red; }' },
        { test: 'bb = border-bottom',       source: '"bb_none"',        expected: '.bb_none { border-bottom: none; }' },
        { test: 'bbs = border-bottom-style', source: '"bbs_solid"',     expected: '.bbs_solid { border-bottom-style: solid; }' },
        { test: 'bbc = border-bottom-color', source: '"bbc_red"',       expected: '.bbc_red { border-bottom-color: red; }' },
        { test: 'bg = background',          source: '"bg_red"',         expected: '.bg_red { background: red; }' },
        { test: 'bgc = background-color',   source: '"bgc_transparent"', expected: '.bgc_transparent { background-color: transparent; }' },
        { test: 'color = color',            source: '"color_green"',    expected: '.color_green { color: green; }' },
        { test: 'ws = white-space',         source: '"ws_nowrap"',      expected: '.ws_nowrap { white-space: nowrap; }' },
        { test: 'fstyle = font-style',      source: '"fstyle_italic"',  expected: '.fstyle_italic { font-style: italic; }' },
        { test: 'va = vertical-align',      source: '"va_middle"',      expected: '.va_middle { vertical-align: middle; }' },
        { test: 'clr = clear',              source: '"clr_both"',       expected: '.clr_both { clear: both; }' },
        { test: 'ls = list-style',          source: '"ls_none"',        expected: '.ls_none { list-style: none; }' },
        { test: 'cursor = cursor',          source: '"cursor_pointer"', expected: '.cursor_pointer { cursor: pointer; }' },
        { test: 'o = overflow',             source: '"o_hidden"',       expected: '.o_hidden { overflow: hidden; }' },
        { test: 'td = text-decoration',     source: '"td_none"',        expected: '.td_none { text-decoration: none; }' },
        { test: 'to = text-overflow',       source: '"to_ellipsis"',    expected: '.to_ellipsis { text-overflow: ellipsis; }' },
        { test: 'ww = word-wrap',           source: '"ww_break-word"',  expected: '.ww_break-word { word-wrap: break-word; }' },
        { test: 'tl = table-layout',        source: '"tl_fixed"',       expected: '.tl_fixed { table-layout: fixed; }' },
        { test: 'trf = transform',          source: '"trf_rotate(7deg)"', expected: '.trf_rotate\\(7deg\\) { transform: rotate(7deg); }' },
        { test: 'trs = transition',         source: '"trs_opacity_1s"', expected: '.trs_opacity_1s { transition: opacity 1s; }' },
        { test: 'zi = z-index',             source: '"zi_2"',           expected: '.zi_2 { z-index: 2; }' },
        { test: 'fw = font-weight',         source: '"fw_400"',         expected: '.fw_400 { font-weight: 400; }' },
        { test: 'ff = font-family',         source: '"ff_sans-serif"',  expected: '.ff_sans-serif { font-family: sans-serif; }' },
        { test: 'tshadow = text-shadow',    source: '"tshadow_0_0_2px_black"', expected: '.tshadow_0_0_2px_black { text-shadow: 0 0 2px black; }' },
        { test: 'bshadow = box-shadow',     source: '"bshadow_1px_1px_2px_black"', expected: '.bshadow_1px_1px_2px_black { box-shadow: 1px 1px 2px black; }' },
        { test: 'op = opacity',             source: '"op_.5"',          expected: '.op_\\.5 { opacity: .5; }' },
        { test: 'ta = text-align',          source: '"ta_center"',      expected: '.ta_center { text-align: center; }' },
        { test: 'ol = outline',             source: '"ol_1px_solid_red"', expected: '.ol_1px_solid_red { outline: 1px solid red; }' },
        { test: 'oy = overflow-y',          source: '"oy_scroll"',      expected: '.oy_scroll { overflow-y: scroll; }' },
        { test: 'ox = overflow-x',          source: '"ox_auto"',        expected: '.ox_auto { overflow-x: auto; }' }

        /*
        { test: '', source: '""', expected: '' },
        { test: '', source: '""', expected: '' },
        { test: '', source: '""', expected: '' },
        { test: '', source: '""', expected: '' },
        */
    ];

    for (let i = 0; i < arr.length; i++) {
        let item = arr[i];

        it(item.test, () => {
            let result = atomic.process(item.source).getCss();
            assert.equal(result, item.expected);
        })
    }
});

describe('pseudoklasser', () => {
    it('ska kunna ange :after', () => {
        let str = "'p_10px:after'";
        let result = atomic.process(str).getCss();
        let expected = '.p_10px\\:after:after { padding: 10px; }';

        assert.equal(result, expected);
    });

    it('ska kunna ange :before', () => {
        let str = "'p_10px:before'";
        let result = atomic.process(str).getCss();
        let expected = '.p_10px\\:before:before { padding: 10px; }';

        assert.equal(result, expected);
    });

    it('ska kunna ange :first-child', () => {
        let str = "'p_10px:first-child'";
        let result = atomic.process(str).getCss();
        let expected = '.p_10px\\:first-child:first-child { padding: 10px; }';

        assert.equal(result, expected);
    });

    it('ska kunna ange :last-child', () => {
        let str = "'p_10px:last-child'";
        let result = atomic.process(str).getCss();
        let expected = '.p_10px\\:last-child:last-child { padding: 10px; }';

        assert.equal(result, expected);
    });

    it('ska kunna ange :nth-child', () => {
        let str = "'p_10px:nth-child(2n)'";
        let result = atomic.process(str).getCss();
        let expected = '.p_10px\\:nth-child\\(2n\\):nth-child(2n) { padding: 10px; }';

        assert.equal(result, expected);
    });

    it('ska kunna ange :hover', () => {
        let str = "'p_10px:hover'";
        let result = atomic.process(str).getCss();
        let expected = '.p_10px\\:hover:hover { padding: 10px; }';

        assert.equal(result, expected);
    });

    it('ska lägga på !important om klassnamn slutar med utropstecken', () => {
        let str = "'p_10px:hover!'";
        let result = atomic.process(str).getCss();
        let expected = '.p_10px\\:hover\\!:hover { padding: 10px !important; }';

        assert.equal(result, expected);
    });
});

describe('responsiva klasser', () => {
    it('ska kunna ange responsiva brytpunkter i px', () => {
        let str = '"m_20px_at700px"';
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (max-width:700px) { .m_20px_at700px { margin: 20px; }}';

        assert.equal(result, expected);
    });

    it('ska kunna ange responsiva brytpunkter i em', () => {
        let str = '"m_20px_at700em"';
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (max-width:700em) { .m_20px_at700em { margin: 20px; }}';

        assert.equal(result, expected);
    });

    it('ska kunna ange responsiva brytpunkter i rem', () => {
        let str = '"m_20px_at700rem"';
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (max-width:700rem) { .m_20px_at700rem { margin: 20px; }}';

        assert.equal(result, expected);
    });

    it('flera responsiva klasser med samma brytpunkt ska hamna i samma media query', () => {
        let str = '"m_20px_at700px p_20px_at700px"';
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (max-width:700px) { .m_20px_at700px { margin: 20px; }.p_20px_at700px { padding: 20px; }}';

        assert.equal(result, expected);
    });

    it('ska sortera responsiva brytpunkter enligt desktop-first som default', () => {
        let str = '"m_20px_at700px m_20px_at300px m_20px_at1000px"';
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (max-width:1000px) { .m_20px_at1000px { margin: 20px; }}@media screen and (max-width:700px) { .m_20px_at700px { margin: 20px; }}@media screen and (max-width:300px) { .m_20px_at300px { margin: 20px; }}';

        assert.equal(result, expected);
    });

    it('ska kunna ange i config att responsiva klasser ska vara mobile-first', () => {
        let str = '"m_20px_at700px m_20px_at300px m_20px_at1000px"';

        atomic.config.mobileFirst = true;

        let result = atomic.process(str).getCss();
        let expected = '@media screen and (min-width:300px) { .m_20px_at300px { margin: 20px; }}@media screen and (min-width:700px) { .m_20px_at700px { margin: 20px; }}@media screen and (min-width:1000px) { .m_20px_at1000px { margin: 20px; }}';

        assert.equal(result, expected);
    });

    it('ska kunna ange pseudoklasser i responsiva klasser', () => {
        let str = "'p_10px:before_at700px'";
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (max-width:700px) { .p_10px\\:before_at700px:before { padding: 10px; }}';

        assert.equal(result, expected);
    });

    it('ska kunna ange mobile-first explicit', () => {
        atomic.config.mobileFirst = false;
        let str = "'p_10px_atMin700px'";
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (min-width:700px) { .p_10px_atMin700px { padding: 10px; }}';

        assert.equal(result, expected);
    });

    it('ska kunna ange desktop-first explicit', () => {
        let str = "'p_10px_atMax700px'";
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (max-width:700px) { .p_10px_atMax700px { padding: 10px; }}';

        assert.equal(result, expected);
    });

    it('explicita mobile-first och desktop-first för samma bredd ska hamna i egna media queries', () => {
        let str = "'p_10px_atMax700px p_10px_atMin700px'";
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (min-width:700px) { .p_10px_atMin700px { padding: 10px; }}@media screen and (max-width:700px) { .p_10px_atMax700px { padding: 10px; }}';

        assert.equal(result, expected);
    });

    it('ska kunna blanda explicita och implicita mobile-first klasser', () => {
        atomic.config.mobileFirst = true;
        let str = "'m_20px_at600px m_10px_at700px p_10px_atMin700px'";
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (min-width:600px) { .m_20px_at600px { margin: 20px; }}@media screen and (min-width:700px) { .m_10px_at700px { margin: 10px; }.p_10px_atMin700px { padding: 10px; }}';

        assert.equal(result, expected);
    });

    it('ska kunna blanda explicita och implicita desktop-first klasser', () => {
        atomic.config.mobileFirst = false;
        let str = "'m_20px_at600px m_10px_at700px p_10px_atMax700px'";
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (max-width:700px) { .m_10px_at700px { margin: 10px; }.p_10px_atMax700px { padding: 10px; }}@media screen and (max-width:600px) { .m_20px_at600px { margin: 20px; }}';

        assert.equal(result, expected);
    });

    it('ska sortera mobile-first och desktop-first brytpunkter var för sig', () => {
        let str = "'m_20px_atMax600px m_10px_atMin700px p_10px_atMax700px p_10px_atMin600px'";
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (min-width:600px) { .p_10px_atMin600px { padding: 10px; }}@media screen and (min-width:700px) { .m_10px_atMin700px { margin: 10px; }}@media screen and (max-width:700px) { .p_10px_atMax700px { padding: 10px; }}@media screen and (max-width:600px) { .m_20px_atMax600px { margin: 20px; }}';

        assert.equal(result, expected);
    });

    it('ska lägga på !important om klassnamn slutar med utropstecken', () => {
        let str = '"m_20px_at700px!"';
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (max-width:700px) { .m_20px_at700px\\! { margin: 20px !important; }}';

        assert.equal(result, expected);
    });

    it('ska kunna använda en variabel i implicit brytpunkt', () => {
        atomic.config.variables = { mobile: '400px' };

        let str = '"m_20px_at@mobile"';
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (max-width:400px) { .m_20px_at\\@mobile { margin: 20px; }}';

        assert.equal(result, expected);
    });

    it('ska kunna ange variabler med versaler', () => {
        atomic.config.variables = { MOB: '400px' };

        let str = '"m_20px_at@MOB"';
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (max-width:400px) { .m_20px_at\\@MOB { margin: 20px; }}';

        assert.equal(result, expected);
    });

    it('ska kunna använda en variabel i explicit mobile-first brytpunkt', () => {
        atomic.config.variables = { tablet: '1024px' };
        
        let str = '"m_20px_atMin@tablet"';
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (min-width:1024px) { .m_20px_atMin\\@tablet { margin: 20px; }}';

        assert.equal(result, expected);
    });

    it('ska kunna använda en variabel i explicit desktop-first brytpunkt', () => {
        atomic.config.variables = { desktop: '1200px' };
        
        let str = '"m_20px_atMax@desktop"';
        let result = atomic.process(str).getCss();
        let expected = '@media screen and (max-width:1200px) { .m_20px_atMax\\@desktop { margin: 20px; }}';

        assert.equal(result, expected);
    });
});