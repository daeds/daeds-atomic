#!/usr/bin/env node

const filewatcher = require('filewatcher');
const getConfig = require('./config.js');
const processFiles = require('./files.js');

const config = getConfig();

if (!config) return;

if (config.verbose){
    console.log('----------------------------------------------------');
    console.log('                    daeds-atomic');
    console.log(' Just another one of those atomic css libraries ;-)');
    console.log('----------------------------------------------------');
}

if (process.argv.length > 2 && (process.argv[2] === '-w' || process.argv[2] === '--watch')) {
    const watcher = filewatcher();

    config.folders.forEach(folder => watcher.add(folder));

    if (config.verbose) {
        console.log('Listening...');
    }

    watcher.on('change', (file, stat) => {
        if (config.verbose) {
            console.log('File change detected in ' + file);
        }

        processFiles();
        
        if (config.verbose) {
            console.log('Listening...');
        }
    });
}
else {
    processFiles();
}