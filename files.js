const filehound = require('filehound');
const Atomic = require('./atomic.js');
const getConfig = require('./config.js');
const fs = require('fs');

const processFiles = function(skipOutputFile){
    const config = getConfig(skipOutputFile);

    if (!config) return;

    const atomic = new Atomic(config);

    return (filehound
        .create()
        .paths(config.folders)
        .ext(config.fileExtensions)
        .find()
        .then(files => {
            if (files && files.length) {
                files.forEach(file => {
                    if (config.verbose) {
                        console.log('Proccessing ' + file);
                    }
                    
                    atomic.process(fs.readFileSync(file, 'utf8'));
                });

                if (!skipOutputFile){
                    if (config.verbose) {
                        console.log('Writing css output to ' + config.output);
                    }

                    fs.writeFileSync(config.output, atomic.getCss().replace(/}/g, '}\n'), { encoding: 'utf8' });
                }
                
                if (config.verbose) {
                    console.log('Done!');
                }
            }

            return atomic; 
        }));
};

module.exports = processFiles;