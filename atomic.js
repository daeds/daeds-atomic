class Atomic {
    constructor(config){
        this.config = config || {};
        this.ruleNames = require('./rule-names.js');
        this.responsiveRegexStr = '_at(?:Min|Max)?(?:(?:@[a-zA-Z0-9]+)|(?:\\d+(?:px|em|rem)))'
        this.responsiveRegex = new RegExp(this.responsiveRegexStr);
        this.valuesRegexStr = '_[\\-a-zA-Z,\\.\\:_\\d\\(\\)\\%#@!]+';
        this.setupOverrides();
        this.classesRegex = this.getRegex();
        this.pseudoRegex = /\:[a-z0-9()\-]+/;
        this.classesFound = {};
        this.mobileFirstClasses = {};
        this.desktopFirstClasses = {};
        this.css = '';
    }

    reset () {
        this.classesFound = {};
        this.mobileFirstClasses = {};
        this.desktopFirstClasses = {};
        this.css = '';
        return this;
    }

    setupOverrides() {
        if (this.config.overrides) {
            for (let name in this.config.overrides) {
                if (this.config.overrides.hasOwnProperty(name)) {
                    this.ruleNames[name] = this.config.overrides[name];
                }
            }
        }
    }

    getRegex() {
        let prefixes = [];

        for (let cls in this.ruleNames) {
            if (this.ruleNames.hasOwnProperty(cls)) {
                prefixes.push(cls);
            }
        }

        return new RegExp(`['" ](?:${ prefixes.join('|') })${ this.valuesRegexStr }(?:${ this.responsiveRegexStr })?`, 'g');
    }

    isClassFound (className) {
        let objectSafeClassName = className
                                    .replace(/%/g, 'pr')
                                    .replace(/\(/g, 'leftpar')
                                    .replace(/\)/g, 'rightpar')
                                    .replace(/,/g, 'comma')
                                    .replace(/\./g, 'dot')
                                    .replace(/\:/g, 'colon')
                                    .replace(/#/g, 'hash')
                                    .replace(/@/g, 'atsign');

        if (objectSafeClassName in this.classesFound) return true;
        this.classesFound[objectSafeClassName] = 1;

        return false;
    }

    getCssForClassName (className) {
        let prefixName = className.match(/^[a-z]+/)[0];
        let rest = className.substr(className.indexOf('_') + 1);
        let pseudoClass = className.match(this.pseudoRegex) || '';
        let isImportant = /!$/.test(className);

        className = className
                        .replace(/%/g, '\\%')
                        .replace(/\(/g, '\\(')
                        .replace(/\)/g, '\\)')
                        .replace(/,/g, '\\,')
                        .replace(/\./g, '\\.')
                        .replace(/\:/g, '\\:')
                        .replace(/#/g, '\\#')
                        .replace(/@/g, '\\@')
                        .replace(/!/g, '\\!');

        let cssValue = rest
                        .replace(this.responsiveRegex, '')
                        .replace(/!/g, '')
                        .replace(/_/g, ' ')
                        .replace(this.pseudoRegex, '')
                        .trim();

        if (this.config.variables) {
            cssValue = this.insertVariablesInCss(cssValue);
        }

        return `.${ className }${ pseudoClass } { ${ this.ruleNames[prefixName] }: ${ cssValue }${ isImportant ? ' !important' : '' }; }`;
    }

    insertVariablesInCss (cssValue) {
        let variablesInClassName = cssValue.match(/@[a-z0-9]+/ig);

        if (variablesInClassName && variablesInClassName.length) {
            variablesInClassName.forEach((varName) => {
                cssValue = cssValue.replace(new RegExp(varName, 'g'), (this.config.variables[varName.replace('@', '')] || ''));
            });
        }

        return cssValue;
    }

    getCss () {
        ['mobileFirstClasses', 'desktopFirstClasses'].forEach(prop => {
            let sortedClasses = [];
            let mobileFirst = (prop === 'mobileFirstClasses');
        
            for (let at in this[prop]) {
                if (this[prop].hasOwnProperty(at)){
                    sortedClasses.push(at);
                }
            }
            
            sortedClasses.sort((a, b) => mobileFirst ? parseInt(a.match(/\d+/)[0], 10) - parseInt(b.match(/\d+/)[0], 10) : parseInt(b.match(/\d+/)[0], 10) - parseInt(a.match(/\d+/)[0], 10));

            sortedClasses.forEach((at) => {
                this.css += `@media screen and (${ mobileFirst ? 'min' : 'max' }-width:${ at.match(/\d+(px|em|rem)/)[0] }) { `;
                    
                this[prop][at].forEach((cls) => {
                    this.css += cls;
                });

                this.css += '}';
            });
        });

        return this.css;
    }

    process(content) {
        let classList = content.match(this.classesRegex);

        if (this.config.alwaysInclude) {
            classList = classList || [];
            classList = classList.concat(this.config.alwaysInclude);
        }

        if (!classList) return this;

        classList.forEach((name) => {
            name = name.replace(/['" ]/g, '');

            if (this.isClassFound(name)) return this;

            let responsiveClass = name.match(this.responsiveRegex);
            let cssClass = this.getCssForClassName(name);

            if (responsiveClass) {
                let breakpointName = responsiveClass[0];

                if (this.config.variables) {
                    breakpointName = this.insertVariablesInCss(breakpointName);
                }

                let isMobileFirst = this.config.mobileFirst;

                if (/Min/.test(breakpointName)) { isMobileFirst = true; }
                if (/Max/.test(breakpointName)) { isMobileFirst = false; }

                let prop = (isMobileFirst ? 'mobileFirstClasses' : 'desktopFirstClasses');
                breakpointName = breakpointName.replace(/(Min|Max)/g, '');

                if (!this[prop][breakpointName]) {
                    this[prop][breakpointName] = [];
                }

                this[prop][breakpointName].push(cssClass);
            }
            else {
                this.css += cssClass;
            }
        });

        return this;
    }
};

module.exports = Atomic;