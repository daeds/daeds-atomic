# daeds-atomic

A small command line utility for using atomic css classes in your markup, heavily influenced from [acss.info](https://acss.io). It will 
create a css file based on the classes found in the folders it monitors.

## Using webpack?
Then try [daeds-atomic-webpack-plugin](https://www.npmjs.com/package/daeds-atomic-webpack-plugin) instead.

## Installation

```shell
npm install -g daeds-atomic
```
## Configuration
The first thing you need to do is to create a file named daeds-atomic.json in the root of your project. 
Below is an example file:
```json
{
    "folders" : [ "/src/views", "/src/templates" ],
    "fileExtensions" : [ "html", "php" ],
    "output" : "/src/styles/atomic.css",
    "mobileFirst" : false,
    "variables" : { 
        "mainColor" : "red",
        "headerPadding" : "20px"
    },
    "overrides" : {
        "custom" : "margin"
    },
    "verbose" : false,
    "alwaysInclude" : ["bgc_@mainColor", "po_static"]
}
```
### folders 
Array where you specify what folders daeds-atomic should listen for file changes in. Mandatory field.

### fileExtensions
Array specifying what file extensions to monitor. Mandatory field.

### output
String containing the relative path (from your root folder) where to put the resulting css file. Mandatory field.

### mobileFirst
Boolean value specifying if your responsive classes should be mobile first or not, if not explicitly set in class name. Defaults to false.

### variables
Hash where you can specify variable names along with an arbritary value. The variable names can be used in class names with an @-prefix.

### overrides
Hash where you can override any of the predefined class prefixes, or add new ones. Based on the example above, you could in your markup
write a class named custom_20px which would result in .custom_20px { margin: 20px; }

### verbose
Boolean value indicating verbose output from the utility.

### alwaysInclude
Array of class names that will always be outputted in the resulting css file, no matter if the classes are found or not.

## Usage
When you have created the config file, just run the following command in your project root and you are ready to go:
```shell
daeds-atomic --watch
```
Add the --watch (or just -w) argument to also monitor for file changes.

## Basics

The principle is that you write your styling directly in your markup as css classes with the following format:
```shell
(class prefix)_(css value)[:(pseudo class)][_at[Min|Max](responsive breakpoint)][!]
```
These classes are picked up and a css file is created automatically, consisting of the classes with their respective css rule.

## Simple example

Say that you want a 20px margin on some element in your markup. To achieve this, use the following class:
```html
<div class="m_20px">Hello world</div>
```
This markup would create a css file with the following content:
```css
.m_20px { margin: 20px; }
```
Let's say you also want a 10px padding on the element + a red background-color when a user hovers over the element. To do this, add the following:
```html
<div class="m_20px p_10px bgc_red:hover">Hello world</div>
```
This would update the css file to look like this:
```css
.m_20px { margin: 20px; }
.p_10px { padding: 10px; }
.bgc_red\:hover:hover { background-color: red; }
```
Finally, you also want the element to have a 50% width when the browser window is less than 700px. Assuming the mobileFirst-property in the config is false, let's add a class for that:
```html
<div class="m_20px p_10px bgc_red:hover w_50%_at700px">Hello world</div>
```
And this is the resulting css:
```css
.m_20px { margin: 20px; }
.p_10px { padding: 10px; }
.bgc_red\:hover:hover { background-color: red; }
@media screen and (max-width:700px) { .w_50\%_at700px { width: 50%; }}
```

## Class prefixes

All atomic classes start with a prefix name in lower case followed by an underscore. The prefix dictates what css property 
name should be used. Below are the default class name prefixes. If you don't like them or if you miss a certain css 
property, you can easily override them or add new ones in the config.
```shell
b = border                  bb = border-bottom              
bbc = border-bottom-color   bbs = border-bottom-style   
bbw = border-bottom-width   bc = border-color
bg = background             bgc = background-color
bl = border-left            blc = border-left-color
bls = border-left-style     blw = border-left-width
bottom = bottom             boxsizing = box-sizing
br = border-right           brc = border-right-color
brs = border-right-style    brw = border-right-width
bs = border-style           bshadow = box-shadow
bt = border-top             btc = border-top-color
bts = border-top-style      btw = border-top-width
bw = border-width           clr = clear
color = color               cursor = cursor
d = display                 f = float
ff = font-family            fs = font-size
fstyle = font-style         fw = font-weight
h = height                  left = left
lh = line-height            ls = list-style
m = margin                  maxh = max-height
maxw = max-width            mb = margin-bottom
minh = min-height           minw = min-width
ml = margin-left            mr = margin-right
mt = margin-top             o = overflow
op = opacity                p = padding
pb = padding-bottom         pl = padding-left
po = position               pr = padding-right
pt = padding-top            radius = border-radius
right = right               td = text-decoration
tl = table-layout           to = text-overflow
top = top                   trf = transform
trs = transition            tshadow = text-shadow
va = vertical-align         w = width
ws = white-space            ww = word-wrap
zi = z-index                ta = text-align
ol = outline                ox = overflow-x
oy = overflow-y
```
## Css values in classes
After the class prefix, just enter any css value. Some key points:
* Use underscores for spaces, for example m_10px_20px will result in .m_10px_20px { margin: 10px 20px; }
* Keep in mind there is no validation what so ever. So if you enter a class called m_blah, that will result in 
.m_blah { margin: blah; } in the css output.
* You can use the following special characters in your class names (which will be escaped in the resulting css file): % . , ( ) @ # :

## Pseudo classes
Pseudo classes are defined as a colon followed by the pseudo class name. For example b_none:last-child or bgc_red:hover.

## Variables
You can define variables in the daeds-atomic.json config file (se example file above for syntax). To use a variable, 
just incude the variable name in a class name, with a @-prefix. So if you for example have a variable named 
standardPadding, you can use it as follows: p_@standardPadding. Keep in
mind that variable names can only contain lowercase and uppercase letters and digits.

## Responsive classes
Add classes defined within responsive breakpoints using the following suffix for the class name (where breakpoint is a variable or a value in px, em or rem): 
```shell
_at[Min|Max](breakpoint)
```
 
### Implicit mobile-first / desktop-first breakpoints
If you leave out the Min or Max in your class name, the mobileFirst-property in the daeds-atomic.json config file will decide if the class will be mobile-first or desktop-first. For example: 
```shell
m_10px_at700px or p_10px_at30em.
```

### Explicit mobile-first / desktop-first breakpoints
If you specify the Min or Max in your class name, it will control if the class will be mobile-first (Min) or desktop-first (Max). For example:
```shell
m_10px_atMin700px or p_10px_atMax30em.
```

### Variable as breakpoint
You can use variables from the config file as breakpoint. For example if you have a variable named "smallScreen" you could use it as follows:
```shell
m_20px_at@smallScreen or m_20px_atMin@smallScreen m_20px_atMax@smallScreen
```

## !important
Add a ! at the end of a class name to make the rule !important. For example: 
```shell
m_20px!
```